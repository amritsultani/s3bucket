#!/bin/bash

## Script 1: Delete AWS S3 bucket hosting static website by Amrit Sultani

if aws s3 ls | grep $1 >/dev/null 2>&1; then
	aws s3 rm s3://$1 --recursive >/dev/null 2>&1
	aws s3api delete-bucket --bucket $1 --region eu-central-1 >/dev/null 2>&1
	if aws s3 ls | grep $1 >/dev/null 2>&1; then
		echo "Can't delete"
	else
		echo $1 "Bucket Purged"
	fi
else
	echo $1 "does not exist"
fi
