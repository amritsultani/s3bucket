# s3Bucket

## Introduction to S3 Buckets

S3 Bucket Task
- How do you create S3 bucket from web ui
- Copy file from your laptop to S3 
- Copy from S3 to your laptop
- Make a static website
- Delete S3 bucket
- Repeat using AWS CLI only - try to automate it too.

Script 1 

- Create S3 bucket
- Make it a website
- Upload pages + picture
- Return URL

Script 2

- Remove bucket
