#!/bin/bash

## Script 1: Create AWS S3 bucket hosting static website by Amrit Sultani

#Create S3 bucket
aws s3api create-bucket --bucket $1 --region eu-central-1 --create-bucket-configuration LocationConstraint=eu-central-1

# Upload pages and pics
aws s3 cp giphy.gif s3://$1/giphy.gif --acl public-read

echo "
<!DOCTYPE html>
<html>
<body>

<h1>Amrit's S3 Bucket</h1>
<p>Hi, my name is Amrit and I own this S3 bucket automated by my script ^_^</p>
<a href="https://s3.eu-central-1.amazonaws.com/$1/giphy.gif">click me</a>

</body>
</html>
">index.html

aws s3 cp index.html s3://$1/index.html --acl public-read

# Make it a static web site
aws s3 website s3://$1/ --index-document index.html

open http://$1.s3-website.eu-central-1.amazonaws.com
